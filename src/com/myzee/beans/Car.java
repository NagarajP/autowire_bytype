package com.myzee.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class Car {
	private String carName;
	@Autowired
	private Engine engine;
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	
	public void printData() {
		System.out.println(this.carName + ":" + this.engine.getModelYear());
	}
}
