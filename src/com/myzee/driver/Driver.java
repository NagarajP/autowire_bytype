package com.myzee.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myzee.beans.Car;

public class Driver {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/myzee/resources/spring.xml");
		Car c = (Car)context.getBean("c");
		c.printData();
	}
}